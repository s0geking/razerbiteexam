<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
class Employee extends Model
{
    use SoftDeletes;
    use ValidatingTrait;

    protected $rules = [
        'first_name'        => 'required',
        'middle_name'       => 'required',
        'last_name'         => 'required',
        'position'          => 'required',
        'mobile_number'     => 'required|max:11|min:11',
        'email'             => 'required|email'
    ];

    protected $date = ['deleted_at'];
    protected $fillable = [

        'first_name',
        'middle_name',
        'last_name',

        'position',
        'mobile_number',
        'email',

    ];
}
