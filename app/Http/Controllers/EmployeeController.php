<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Employee;

class EmployeeController extends Controller
{

    public function index()
    {
        $employee = Employee::orderBy('id')->get();
        return view('employee.index', compact('employee'));
    }

    public function show(Employee $employee)
    {
        return view('employee.show', compact('employee'));
    }

    public function create()
    {
        $max_id = Employee::withTrashed()->max('id') + 1;
        return view('employee.create', compact('max_id'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $address = $input['street'] . ' ' . $input['barangay'] . ' ' . $input['city'] . ' ' .$input['zip'];

        $employee = new Employee();
        $employee->current_address = $address;
        $employee->fill($input);

        if(! $employee->save())
        {
            $request->session()->flash('fail', "Ooops there are problems with the request.");
            return back()->withInput()->withErrors($employee->getErrors());
        }

        $name = $input['last_name'] . ', ' . $input['first_name'] . ' ' . $input['middle_name'];
        $request->session()->flash('success', "$name Successfully added.");

        return redirect()->action('EmployeeController@index');
    }

    public function edit(Employee $employee)
    {
        return view('employee.edit', compact('employee'));
    }

    public function update(Request $request, Employee $employee)
    {
        $input = $request->all();

        $employee = Employee::find($employee->id);
        $employee->current_address = $input['address'];
        $employee->fill($input);

        if(! $employee->save())
        {
            $request->session()->flash('fail', "Ooops there are problems with the request.");
            return back()->withInput()->withErrors($employee->getErrors());
        }
        $request->session()->flash('success', "Employee # $employee->id Successfully updated.");
        return redirect()->action('EmployeeController@index');
    }

    public function destroy(Request $request, Employee $employee)
    {
        if(!$employee->delete())
        {
            $request->session()->flash('fail', "Ooops there are problems with the request.");
            return back();
        }

        $request->session()->flash('success', "Successfully deleted Employee #$employee->id");
        return back();
    }

    public function DeactivatedEmployees()
    {
        $employee = Employee::onlyTrashed()->get();
        $noAction = true;
        return view('employee.index', compact('employee', 'noAction'));
    }
}
