<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Employee;




Route::get('/employees', 'EmployeeController@index');


Route::get('/employees/create', 'EmployeeController@create');
Route::post('/employees', 'EmployeeController@store');

Route::get('/employees/deactivated', 'EmployeeController@DeactivatedEmployees');

Route::get('/employees/{employee}/edit', 'EmployeeController@edit');
Route::put('employees/{employee}', 'EmployeeController@update');

Route::delete('/employees/{employee}', 'EmployeeController@destroy');
Route::get('/employees/{employee}', 'EmployeeController@show');

Route::get('/', function () {
    return  App::environment();
});
