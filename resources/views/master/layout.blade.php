<html>
<head>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    @yield('head')

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false,
                } ]
            });
        } );

        $('.nav li').click(function(){
            $('.nav li').removeClass('active');
            $(this).addClass('active');
        })
    </script>
    <style>
        .row {
            zoom: 1;
            padding-top: 2em;
            margin-left: -20px;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Exam @ RazerBite</a>
        </div>
    </div>
</nav>

<div class="row" style="margin: 2em">
    <div class="col-md-2">
        <div class="well">
            <ul id="awts" class="nav nav-pills nav-stacked">
                <li>{!! link_to('/employees/','Employees') !!}</li>
                <li>{!! link_to('/employees/create','Add Employee') !!}</li>
                <li>{!! link_to('/employees/deactivated','Deactivated Employees') !!}</li>
            </ul>
        </div>
    </div>
    <div class="col-md-10">
        <div class="panel panel-primary">
            <div class="panel-heading">@yield('page_title')</div>
            <div class="panel-body">
                @yield('body')
            </div>
        </div>
    </div>
</div>
</body>

</html>