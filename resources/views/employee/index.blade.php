@extends('master/layout')

@section('page_title')
    List of Employees
@stop

@section('body')
    @if(Session::has('success'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            {{ Session::get('success') }}
        </div>
    @endif

    <table id="example" class="table table-striped table-bordere" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th width="5%">#</th>
            <th width="20%">Name</th>
            <th width="10%">Position</th>
            <th width="10%">Mobile #</th>
            <th width="10%">Email</th>
            <th width="20%">Address</th>
            @if(!isset($noAction))
                <th class="no-sort" width="10%">Actions</th>
            @endif
        </tr>
        </thead>

        <tbody>
        @foreach($employee as $employees)
            <?php $name = $employees->last_name . ", " . $employees->first_name . " " . $employees->middle_name; ?>
            <tr>
                <td>{{$employees->id}}</td>
                <td>{{$name}}</td>
                <td>{{$employees->position}}</td>
                <td>{{$employees->mobile_number}}</td>
                <td>{{$employees->email}}</td>
                <td>{{$employees->current_address}}</td>
                @if(!isset($noAction))
                    <td>
                        {!! Form::open(['method' => 'get', 'action' => ['EmployeeController@show', $employees->id]]) !!}
                            <button type="submit" class="btn btn-success btn-sm">
                                <span class="glyphicon glyphicon-heart pull-left"></span> &nbsp;&nbsp;VIEW&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </button>
                        {!! Form::close() !!}
                        <hr>
                         {!! Form::open(['method' => 'delete', 'action' => ['EmployeeController@destroy', $employees->id]]) !!}

                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete {{$name}}')">
                                <span class="glyphicon glyphicon-trash pull-left"></span> &nbsp;&nbsp;DELETE&nbsp;&nbsp;
                            </button>

                         {!! Form::close() !!}
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
@stop
