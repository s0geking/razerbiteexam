@extends('master/layout')

@section('header')

@stop

@section('page_title')
    Add Employee
@stop

@section('body')
    @if(Session::has('fail'))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            {{ Session::get('fail') }}
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $error)
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                {{$error}}
                <br>
            @endforeach
        </div>
    @endif
    {!! Form::open(['method' => 'put','action' => ['EmployeeController@update', $employee->id]]) !!}
    <div class="form-group">
        <div class="row">
            <div class="col-md-2">
                {!! Form::label('id','ID: ') !!}
                {!! Form::text('id', $employee->id,['readonly' => 'true', 'class' => 'form-control']) !!}
            </div>

            <div class="col-md-4">
                {!! Form::label('last_name','Last Name: ') !!}
                {!! Form::text('last_name', $employee->last_name,['class' => 'form-control addEmpForm', 'required' => 'true']) !!}
            </div>

            <div class="col-md-3">
                {!! Form::label('first_name','First Name: ') !!}
                {!! Form::text('first_name', $employee->first_name,['class' => 'form-control addEmpForm', 'required' => 'true']) !!}
            </div>

            <div class="col-md-3">
                {!! Form::label('middle_name','Middle Name: ') !!}
                {!! Form::text('middle_name', $employee->middle_name,['class' => 'form-control addEmpForm', 'required' => 'true']) !!}
            </div>
        </div>
    </div>

    <div class="form-group">

        <div class="row">
            <div class="col-md-12">
                {!! Form::label('address','Address: ') !!}
                {!! Form::text('address', $employee->current_address,[  'class' => 'form-control addEmpForm',
                                                                        'required' => 'true',
                                                                        'placeholder' => 'Street #, Barangay, City, Zip Code']) !!}
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-md-4">
                {!! Form::label('position','Position: ') !!}
                {!! Form::text('position', $employee->position,['class' => 'form-control addEmpForm', 'required' => 'true']) !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('mobile_number','Mobile Number: ') !!}
                {!! Form::input('number','mobile_number', '',['class' => 'form-control addEmpForm', 'min' => '0', 'required' => 'true']) !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('email','Email Address: ') !!}
                {!! Form::text('email', $employee->email,['class' => 'form-control addEmpForm', 'required' => 'true']) !!}
            </div>

        </div>
    </div>
    <div class="row">
        <hr>
        <div class="col-md-2 pull-right">
            {!! Form::submit('Update',['class' => 'btn btn-warning']) !!}

        </div>
        <div class="col-md-1 pull-right">
            <button type="button" class="btn btn-default" onclick="$('.addEmpForm').val('');">Clear</button>
        </div>
    </div>
    {!! Form::close() !!}

@stop
