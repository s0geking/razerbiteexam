@extends('master/layout')

@section('header')

@stop

@section('page_title')
    Add Employee
@stop

@section('body')
    @if(Session::has('fail'))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            {{ Session::get('fail') }}
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $error)
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                {{$error}}
                <br>
            @endforeach
        </div>
    @endif

    <div class="row">

        <div class="col-md-8 pull-right">

            <table border="0" >
                <tr>
                    <td><b>Employee #: </b></td>
                    <td>{{$employee->id}}</td>
                </tr>
                <tr>
                    <?php $name = $employee->last_name . ", " . $employee->first_name . " " . $employee->middle_name; ?>
                    <td><b>Name: </b></td>
                    <td>{{$employee->name}}</td>
                </tr>
                <tr>
                    <td><b>Position: </b></td>
                    <td>{{$employee->position}}</td>
                </tr>
                <tr>
                    <td><b>Address: </b></td>
                    <td>{{$employee->current_address}}</td>
                </tr>
                <tr>
                    <td><b>Mobile #: </b></td>
                    <td>{{$employee->mobile_number}}</td>
                </tr>
                <tr>
                    <td><b>Email Address: </b></td>
                    <td>{{$employee->email}}</td>
                </tr>
            </table>

        </div><div class="col-md-3 pull-right"><img src="http://placehold.it/200x200"></div>

    </div>

    <div class="row">
        <hr>
        <div class="col-md-2 pull-right">
            {!! Form::open(['method' => 'get', 'action' => ['EmployeeController@edit', $employee->id]]) !!}
            <button type="submit" class="btn btn-warning btn-sm">
                <span class="glyphicon glyphicon-pencil pull-left"></span> &nbsp;&nbsp;EDIT&nbsp;&nbsp;
            </button>
            {!! Form::close() !!}
        </div>
        <div class="col-md-1 pull-right">
            {!! Form::open(['method' => 'delete', 'action' => ['EmployeeController@destroy', $employee->id]]) !!}

            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete {{$name}}')">
                <span class="glyphicon glyphicon-trash pull-left"></span> &nbsp;&nbsp;DELETE&nbsp;&nbsp;
            </button>

            {!! Form::close() !!}
        </div>
    </div>


@stop
